class CartItemsController < ApplicationController
  def create
    product = Product.find(params[:product_id])
    cart = current_cart
    cart.cartitem.create(cart_id: cart.id, product_id: product.id, qty: params[:qty])
    redirect_to products_path
  end
  
  def destroy
    product = Product.find(params[:id])
    cart = current_cart
    cart.cartitem.find_by(product_id: product.id).destroy
    redirect_to cart_path(current_cart.id)
  end
end
