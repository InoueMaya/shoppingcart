class CreateCartitems < ActiveRecord::Migration
  def change
    create_table :cartitems do |t|
      t.integer :product_id
      t.integer :cart_id
      t.integer :qty

      t.timestamps null: false
    end
  end
end
